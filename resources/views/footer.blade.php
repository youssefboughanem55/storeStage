<footer class="footer mt-auto py-3 bg-transparent fixed-bottom">
  <div class="container">
    <h1 class="text-white">Footer</h1>
    <div class="row">
      <div class="col-md-6">
        <h5 class="text-white">Follow Us</h5>
        <ul class="list-inline">
          <li class="list-inline-item"><a href="#"><i class="fab fa-facebook"></i></a></li>
          <li class="list-inline-item"><a href="#"><i class="fab fa-twitter"></i></a></li>
          <li class="list-inline-item"><a href="#"><i class="fab fa-instagram"></i></a></li>
        </ul>
      </div>
      <div class="col-md-6">
        <h5 class="text-white">Additional Links</h5>
        <ul class="list-unstyled">
          <li><a href="#" class="text-white">About Us</a></li>
          <li><a href="#" class="text-white">Contact Us</a></li>
          <li><a href="#" class="text-white">Privacy Policy</a></li>
        </ul>
      </div>
    </div>
  </div>
</footer>
